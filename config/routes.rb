Rails.application.routes.draw do
  root 'tweets#index'
  post '/tweets/', to: 'tweets#create'
  delete '/tweet/:id(.:format)', to: 'tweets#delete', as: 'tweet'
end
