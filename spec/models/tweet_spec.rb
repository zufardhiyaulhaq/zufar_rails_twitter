require 'rails_helper'

describe Tweet, type: :model do
  context 'given empty parameter' do
    it 'should false' do
      tweet = Tweet.new.save
      expect(tweet).to eql(false)
    end
  end

  context 'given more than 140 character' do
    it 'should false' do
      tweet = Tweet.new(content: '140 character 140 character 140 '\
        'character 140 character 140 character 140 character 140 '\
        'character 140 character 140 character 140 character 1').save
      expect(tweet).to eql(false)
    end
  end
end
