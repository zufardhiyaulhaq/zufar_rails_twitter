## Description
A Simple web like twitter that can create tweet, delete and see the tweets.

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

Install all dependency using bundle in root of directory project.
```bash
bundle install
```

## Run Test
using rspec & rubocop
```bash
bundle exec rake
```

to open the coverage result in browser
```bash
bundle exec rake coverage
```

## Run Instruction
set database environment to match your environment application
- Production environment

| Config | Description |
|-|-|
| RAILS_ENV | production |
| PROD_DATABASE_ADAPTER | production database adapter |
| PROD_DATABASE_NAME | production database name |
| PROD_DATABASE_USERNAME | production database username |
| PROD_DATABASE_PASSWORD | production database password |
| PROD_DATABASE_HOST | production database ip address |
| SECRET_KEY_BASE | production environment need to set the key, fill with long string |

- Test environment

| Config | Description |
|-|-|
| RAILS_ENV | test |
| TEST_DATABASE_ADAPTER | test database adapter |
| TEST_DATABASE_NAME | test database name |
| TEST_DATABASE_USERNAME | test database username |
| TEST_DATABASE_PASSWORD | test database password |
| TEST_DATABASE_HOST | test database ip address |

- Development environment

| Config | Description |
|-|-|
| RAILS_ENV | development |
| DEV_DATABASE_ADAPTER | development database adapter |
| DEV_DATABASE_NAME | development database name |
| DEV_DATABASE_USERNAME | development database username |
| DEV_DATABASE_PASSWORD | development database password |
| DEV_DATABASE_HOST | development database ip address |

migrate the database
```bash
bundle exec rails db:migrate
```

to run the application
```bash
bundle exec rails server
```

open browser and open the app
```bash
http://127.0.0.1:3000
```

## Deployment
Requirement:
- [Vagrant](https://www.vagrantup.com/docs/installation/)
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Docker](https://docs.docker.com/install/)

### Installation
- Create environment virtual machine with Vagrant.
```
vagrant up
```

This will create the following virtual machine:

| Name | IP Address | Description |
|-|-|-|
| db | 192.168.50.10 | Database VM |
| web-1 | 192.168.50.21 | Web service VM |
| web-2 | 192.168.50.22 | Web service VM |
| web-3 | 192.168.50.23 | Web service VM |
| lb | 192.168.50.23 | Load Balancer VM  |
| gitlab-runner | 192.168.50.100 | Runner VM |

- Generate ssh keypair in ansible node if necessary
```
ssh-keygen
```

- Copy ansible node public key to each VM
```
ssh-copy-id USER@IP
```
make sure each user in all VM have sudoers access

- Change configuration in `provisioning/group_vars/all.yml`

| Config | Description |
|-|-|
| postgresql.version | version of postgresql |
| postgresql.host | postgresql IP Address |
| postgresql.database | postgresql Database name |
| postgresql.user | postgresql Database user |
| postgresql.password | postgresql Database password |
| postgresql.allowed_hosts | network allowed to access database |
| ruby.version | ruby version |
| ruby.download_url | ruby download file, get url from [official source](https://www.ruby-lang.org/en/downloads/) |
| bundler.version | bundler version |
| app.user | user craeted for running rails |
| app.directory | directory created to store the rails app |
| app.name | rails app name |
| app.port | rails app port exposed |
| app.master_key | master_key for the rails app |

- Change inventory in `provisioning/hosts/hosts`
- Provisioning & preparing web, database, load balancer, and gitlab-runner machine
```
ansible-playbook -i provisioning/hosts/hosts provisioning/installation.yml
```

### Deployment CI & CD
- Generate ssh keypair in gitlab-runner node if necessary
```
ssh-keygen
```

- Copy gitlab-runner node public key to each VM
```
ssh-copy-id USER@IP
```
make sure each user in all VM have sudoers access

- change apps code and push to repository
